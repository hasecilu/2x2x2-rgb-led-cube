EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "RGB LED CUBE 2x2x2"
Date "2021-07-02"
Rev "1.1"
Comp "@hasecilu"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED_PAD D1
U 1 1 5D4E5282
P 3725 3225
F 0 "D1" H 3725 3505 50  0000 C CNN
F 1 "LED_PAD" H 3725 3414 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm-3" H 3725 3225 50  0001 C CNN
F 3 "~" H 3725 3225 50  0001 C CNN
	1    3725 3225
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_PAD D2
U 1 1 5D4E5AB7
P 3725 2025
F 0 "D2" H 3725 2305 50  0000 C CNN
F 1 "LED_PAD" H 3725 2214 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm-3" H 3725 2025 50  0001 C CNN
F 3 "~" H 3725 2025 50  0001 C CNN
	1    3725 2025
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_PAD D4
U 1 1 5D4E603B
P 3925 2325
F 0 "D4" H 3925 2605 50  0000 C CNN
F 1 "LED_PAD" H 3925 2514 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm-3" H 3925 2325 50  0001 C CNN
F 3 "~" H 3925 2325 50  0001 C CNN
	1    3925 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_PAD D3
U 1 1 5D4E64F8
P 3925 1325
F 0 "D3" H 3925 1605 50  0000 C CNN
F 1 "LED_PAD" H 3925 1514 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm-3" H 3925 1325 50  0001 C CNN
F 3 "~" H 3925 1325 50  0001 C CNN
	1    3925 1325
	1    0    0    -1  
$EndComp
$Comp
L RGB_LED_CUBE_2-rescue:ATmega8-16AU-MCU_Microchip_ATmega U1
U 1 1 5D4E8831
P 2025 2425
F 0 "U1" H 1775 3775 50  0000 C CNN
F 1 "ATmega8-16AU" H 2525 3800 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 2025 2425 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2486-8-bit-avr-microcontroller-atmega8_l_datasheet.pdf" H 2025 2425 50  0001 C CNN
	1    2025 2425
	1    0    0    -1  
$EndComp
Wire Wire Line
	2125 3825 2125 3875
Wire Wire Line
	2125 3875 2075 3875
Wire Wire Line
	2025 3875 2025 3825
$Comp
L power:GND #PWR03
U 1 1 5D5200E9
P 2075 3975
F 0 "#PWR03" H 2075 3725 50  0001 C CNN
F 1 "GND" H 2080 3802 50  0000 C CNN
F 2 "" H 2075 3975 50  0001 C CNN
F 3 "" H 2075 3975 50  0001 C CNN
	1    2075 3975
	1    0    0    -1  
$EndComp
Wire Wire Line
	2075 3975 2075 3875
Connection ~ 2075 3875
Wire Wire Line
	2075 3875 2025 3875
Wire Wire Line
	3425 3325 3425 3075
Wire Wire Line
	3925 3075 3925 3225
Wire Wire Line
	3925 3225 3875 3225
Wire Wire Line
	4125 1325 4075 1325
Wire Wire Line
	3425 2425 3425 2575
Wire Wire Line
	3425 2575 4125 2575
Wire Wire Line
	4125 2575 4125 2325
Wire Wire Line
	4125 2325 4075 2325
$Comp
L Device:R R4
U 1 1 5D5394B5
P 3125 3225
F 0 "R4" V 3125 3225 50  0000 C CNN
F 1 "220" V 3009 3225 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 3225 50  0001 C CNN
F 3 "~" H 3125 3225 50  0001 C CNN
	1    3125 3225
	0    1    1    0   
$EndComp
Wire Wire Line
	3275 3225 3575 3225
Wire Wire Line
	3275 3325 3425 3325
Wire Wire Line
	3275 3425 3725 3425
Wire Wire Line
	3275 1325 3775 1325
Wire Wire Line
	3275 1425 3475 1425
Wire Wire Line
	3275 1525 3925 1525
Wire Wire Line
	3275 2025 3575 2025
Wire Wire Line
	3275 2125 3475 2125
Wire Wire Line
	3275 2225 3725 2225
Wire Wire Line
	3275 2325 3775 2325
Wire Wire Line
	3275 2425 3425 2425
Wire Wire Line
	3275 2525 3925 2525
$Comp
L Device:R R5
U 1 1 5D54F497
P 3125 3325
F 0 "R5" V 3125 3325 50  0000 C CNN
F 1 "220" V 3009 3325 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 3325 50  0001 C CNN
F 3 "~" H 3125 3325 50  0001 C CNN
	1    3125 3325
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5D5509D2
P 3125 3425
F 0 "R6" V 3125 3425 50  0000 C CNN
F 1 "220" V 3009 3425 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 3425 50  0001 C CNN
F 3 "~" H 3125 3425 50  0001 C CNN
	1    3125 3425
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5D5509D8
P 3125 1325
F 0 "R7" V 3125 1325 50  0000 C CNN
F 1 "220" V 3009 1325 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 1325 50  0001 C CNN
F 3 "~" H 3125 1325 50  0001 C CNN
	1    3125 1325
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5D551AC3
P 3125 1425
F 0 "R8" V 3125 1425 50  0000 C CNN
F 1 "220" V 3009 1425 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 1425 50  0001 C CNN
F 3 "~" H 3125 1425 50  0001 C CNN
	1    3125 1425
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5D551AC9
P 3125 1525
F 0 "R9" V 3125 1525 50  0000 C CNN
F 1 "220" V 3009 1525 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 1525 50  0001 C CNN
F 3 "~" H 3125 1525 50  0001 C CNN
	1    3125 1525
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 5D55425C
P 3125 2025
F 0 "R10" V 3125 2025 50  0000 C CNN
F 1 "220" V 3009 2025 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 2025 50  0001 C CNN
F 3 "~" H 3125 2025 50  0001 C CNN
	1    3125 2025
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 5D554262
P 3125 2125
F 0 "R11" V 3125 2125 50  0000 C CNN
F 1 "220" V 3009 2125 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 2125 50  0001 C CNN
F 3 "~" H 3125 2125 50  0001 C CNN
	1    3125 2125
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5D554268
P 3125 2225
F 0 "R12" V 3125 2225 50  0000 C CNN
F 1 "220" V 3009 2225 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 2225 50  0001 C CNN
F 3 "~" H 3125 2225 50  0001 C CNN
	1    3125 2225
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 5D55426E
P 3125 2325
F 0 "R13" V 3125 2325 50  0000 C CNN
F 1 "220" V 3009 2325 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 2325 50  0001 C CNN
F 3 "~" H 3125 2325 50  0001 C CNN
	1    3125 2325
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 5D554274
P 3125 2425
F 0 "R14" V 3125 2425 50  0000 C CNN
F 1 "220" V 3009 2425 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 2425 50  0001 C CNN
F 3 "~" H 3125 2425 50  0001 C CNN
	1    3125 2425
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 5D55427A
P 3125 2525
F 0 "R15" V 3125 2525 50  0000 C CNN
F 1 "220" V 3009 2525 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3055 2525 50  0001 C CNN
F 3 "~" H 3125 2525 50  0001 C CNN
	1    3125 2525
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5D55698C
P 725 1125
F 0 "R1" V 725 1125 50  0000 C CNN
F 1 "10k" V 609 1125 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 655 1125 50  0001 C CNN
F 3 "~" H 725 1125 50  0001 C CNN
	1    725  1125
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5D55A89A
P 7075 1400
F 0 "J1" H 7125 1600 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 7125 1626 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 7075 1400 50  0001 C CNN
F 3 "~" H 7075 1400 50  0001 C CNN
	1    7075 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5D55C2ED
P 7525 1550
F 0 "#PWR05" H 7525 1300 50  0001 C CNN
F 1 "GND" H 7530 1377 50  0000 C CNN
F 2 "" H 7525 1550 50  0001 C CNN
F 3 "" H 7525 1550 50  0001 C CNN
	1    7525 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7375 1500 7525 1500
Wire Wire Line
	7525 1500 7525 1550
Wire Wire Line
	7375 1300 7525 1300
Wire Wire Line
	7375 1400 7525 1400
Wire Wire Line
	6875 1300 6725 1300
Wire Wire Line
	6875 1400 6725 1400
Wire Wire Line
	6875 1500 6725 1500
Text Label 7525 1400 0    50   ~ 0
MOSI
Text Label 6725 1300 0    50   ~ 0
MISO
Text Label 6725 1400 0    50   ~ 0
SCK
Text Label 6725 1500 0    50   ~ 0
RST
Wire Wire Line
	2625 3225 2975 3225
Wire Wire Line
	2625 3325 2975 3325
Wire Wire Line
	2625 3425 2975 3425
Wire Wire Line
	2625 2025 2975 2025
Wire Wire Line
	2625 2125 2975 2125
Wire Wire Line
	2625 2225 2975 2225
Wire Wire Line
	2625 2325 2975 2325
Wire Wire Line
	2625 2425 2975 2425
Wire Wire Line
	2625 2525 2975 2525
Text Label 2725 1625 0    50   ~ 0
MOSI
Text Label 2725 1725 0    50   ~ 0
MISO
Text Label 2725 1825 0    50   ~ 0
SCK
Text Label 1225 1325 0    50   ~ 0
RST
Wire Wire Line
	1425 1525 1125 1525
Wire Wire Line
	1125 1725 1425 1725
NoConn ~ 1425 2225
NoConn ~ 1425 2125
NoConn ~ 1425 1925
Wire Wire Line
	2025 875  2025 1025
NoConn ~ 2125 1025
$Comp
L Transistor_FET:2N7002E Q1
U 1 1 5DC8CFA9
P 5300 3100
F 0 "Q1" H 5506 3146 50  0000 L CNN
F 1 "2N7002E" H 5506 3055 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5500 3025 50  0001 L CIN
F 3 "http://www.diodes.com/assets/Datasheets/ds30376.pdf" H 5300 3100 50  0001 L CNN
	1    5300 3100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5DCB7A9F
P 2025 875
F 0 "#PWR0101" H 2025 725 50  0001 C CNN
F 1 "+5V" H 2040 1048 50  0000 C CNN
F 2 "" H 2025 875 50  0001 C CNN
F 3 "" H 2025 875 50  0001 C CNN
	1    2025 875 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5DCB7C08
P 10350 1225
F 0 "#PWR0102" H 10350 1075 50  0001 C CNN
F 1 "+5V" H 10365 1398 50  0000 C CNN
F 2 "" H 10350 1225 50  0001 C CNN
F 3 "" H 10350 1225 50  0001 C CNN
	1    10350 1225
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 1325 10350 1275
Wire Wire Line
	10350 1625 10350 1675
$Comp
L power:GND #PWR0103
U 1 1 5DCBD2E1
P 10350 1725
F 0 "#PWR0103" H 10350 1475 50  0001 C CNN
F 1 "GND" H 10355 1552 50  0000 C CNN
F 2 "" H 10350 1725 50  0001 C CNN
F 3 "" H 10350 1725 50  0001 C CNN
	1    10350 1725
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 5DC8BBC1
P 6000 3400
F 0 "J2" V 5918 3480 50  0000 L CNN
F 1 "Conn_01x01" V 5963 3480 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 6000 3400 50  0001 C CNN
F 3 "~" H 6000 3400 50  0001 C CNN
	1    6000 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3300 5400 3400
$Comp
L Transistor_FET:2N7002E Q2
U 1 1 5DC96496
P 7250 3125
F 0 "Q2" H 7456 3171 50  0000 L CNN
F 1 "2N7002E" H 7456 3080 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7450 3050 50  0001 L CIN
F 3 "http://www.diodes.com/assets/Datasheets/ds30376.pdf" H 7250 3125 50  0001 L CNN
	1    7250 3125
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 5DC9649C
P 7900 3425
F 0 "J3" V 7818 3505 50  0000 L CNN
F 1 "Conn_01x01" V 7863 3505 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 7900 3425 50  0001 C CNN
F 3 "~" H 7900 3425 50  0001 C CNN
	1    7900 3425
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 3325 7350 3425
Text Label 5400 3400 0    50   ~ 0
AN1
Text Label 7350 3425 0    50   ~ 0
AN2
$Comp
L Device:R R17
U 1 1 5DC99D77
P 5400 3650
F 0 "R17" H 5470 3696 50  0000 L CNN
F 1 "10k" H 5470 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5330 3650 50  0001 C CNN
F 3 "~" H 5400 3650 50  0001 C CNN
	1    5400 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5DCA1CCD
P 5400 3800
F 0 "#PWR0104" H 5400 3550 50  0001 C CNN
F 1 "GND" H 5405 3627 50  0000 C CNN
F 2 "" H 5400 3800 50  0001 C CNN
F 3 "" H 5400 3800 50  0001 C CNN
	1    5400 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3400 5400 3400
Connection ~ 5400 3400
Wire Wire Line
	5400 3400 5400 3500
Wire Wire Line
	7700 3425 7350 3425
Connection ~ 7350 3425
Wire Wire Line
	7350 3425 7350 3525
$Comp
L power:GND #PWR0105
U 1 1 5DCA9649
P 7350 3825
F 0 "#PWR0105" H 7350 3575 50  0001 C CNN
F 1 "GND" H 7355 3652 50  0000 C CNN
F 2 "" H 7350 3825 50  0001 C CNN
F 3 "" H 7350 3825 50  0001 C CNN
	1    7350 3825
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 5DCACA63
P 4950 3100
F 0 "R16" V 4743 3100 50  0000 C CNN
F 1 "1k" V 4834 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4880 3100 50  0001 C CNN
F 3 "~" H 4950 3100 50  0001 C CNN
	1    4950 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R18
U 1 1 5DCC1062
P 6900 3125
F 0 "R18" V 6693 3125 50  0000 C CNN
F 1 "1k" V 6784 3125 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6830 3125 50  0001 C CNN
F 3 "~" H 6900 3125 50  0001 C CNN
	1    6900 3125
	0    1    1    0   
$EndComp
$Comp
L Device:R R19
U 1 1 5DCC13FF
P 7350 3675
F 0 "R19" H 7420 3721 50  0000 L CNN
F 1 "10k" H 7420 3630 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7280 3675 50  0001 C CNN
F 3 "~" H 7350 3675 50  0001 C CNN
	1    7350 3675
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5DCC3F5E
P 5400 2900
F 0 "#PWR0106" H 5400 2750 50  0001 C CNN
F 1 "+5V" H 5415 3073 50  0000 C CNN
F 2 "" H 5400 2900 50  0001 C CNN
F 3 "" H 5400 2900 50  0001 C CNN
	1    5400 2900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 5DCC5A7B
P 7350 2925
F 0 "#PWR0107" H 7350 2775 50  0001 C CNN
F 1 "+5V" H 7365 3098 50  0000 C CNN
F 2 "" H 7350 2925 50  0001 C CNN
F 3 "" H 7350 2925 50  0001 C CNN
	1    7350 2925
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3125 6500 3125
Text Label 6500 3125 0    50   ~ 0
GATE2
Text Label 4550 3100 0    50   ~ 0
GATE1
Wire Wire Line
	4550 3100 4800 3100
Text Label 1125 1525 0    50   ~ 0
GATE1
Text Label 1125 1725 0    50   ~ 0
GATE2
$Comp
L power:+5V #PWR0108
U 1 1 5DD22BA4
P 725 875
F 0 "#PWR0108" H 725 725 50  0001 C CNN
F 1 "+5V" H 740 1048 50  0000 C CNN
F 2 "" H 725 875 50  0001 C CNN
F 3 "" H 725 875 50  0001 C CNN
	1    725  875 
	1    0    0    -1  
$EndComp
$Comp
L RGB_LED_CUBE_2-rescue:USB_B_Micro-Connector J4
U 1 1 5DD287DE
P 5150 1425
F 0 "J4" H 5207 1892 50  0000 C CNN
F 1 "USB_B_Micro" H 5207 1801 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex-105017-0001" H 5300 1375 50  0001 C CNN
F 3 "~" H 5300 1375 50  0001 C CNN
	1    5150 1425
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0109
U 1 1 5DD2A7B1
P 5600 1175
F 0 "#PWR0109" H 5600 1025 50  0001 C CNN
F 1 "+5V" H 5615 1348 50  0000 C CNN
F 2 "" H 5600 1175 50  0001 C CNN
F 3 "" H 5600 1175 50  0001 C CNN
	1    5600 1175
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 1225 5600 1225
Wire Wire Line
	5600 1225 5600 1175
NoConn ~ 5450 1425
NoConn ~ 5450 1525
NoConn ~ 5050 1825
NoConn ~ 5450 1625
$Comp
L power:GND #PWR0110
U 1 1 5DD344C9
P 5150 1925
F 0 "#PWR0110" H 5150 1675 50  0001 C CNN
F 1 "GND" H 5155 1752 50  0000 C CNN
F 2 "" H 5150 1925 50  0001 C CNN
F 3 "" H 5150 1925 50  0001 C CNN
	1    5150 1925
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5DD4189E
P 10050 1475
F 0 "C1" H 10165 1521 50  0000 L CNN
F 1 "100n" H 10165 1430 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 10088 1325 50  0001 C CNN
F 3 "~" H 10050 1475 50  0001 C CNN
	1    10050 1475
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 1325 10050 1275
Wire Wire Line
	10650 1275 10650 1325
Wire Wire Line
	10050 1275 10350 1275
Connection ~ 10350 1275
Wire Wire Line
	10350 1275 10350 1225
Wire Wire Line
	10350 1275 10650 1275
Wire Wire Line
	10050 1625 10050 1675
Wire Wire Line
	10050 1675 10350 1675
Connection ~ 10350 1675
Wire Wire Line
	10350 1675 10350 1725
Wire Wire Line
	10350 1675 10650 1675
Wire Wire Line
	10650 1675 10650 1625
$Comp
L Device:C C2
U 1 1 5DD4A9B3
P 10350 1475
F 0 "C2" H 10465 1521 50  0000 L CNN
F 1 "100n" H 10465 1430 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 10388 1325 50  0001 C CNN
F 3 "~" H 10350 1475 50  0001 C CNN
	1    10350 1475
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5DD4AB54
P 10650 1475
F 0 "C3" H 10765 1521 50  0000 L CNN
F 1 "100n" H 10765 1430 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 10688 1325 50  0001 C CNN
F 3 "~" H 10650 1475 50  0001 C CNN
	1    10650 1475
	1    0    0    -1  
$EndComp
Wire Wire Line
	2625 1625 2725 1625
Wire Wire Line
	2625 1725 2725 1725
Wire Wire Line
	2625 1825 2725 1825
Wire Wire Line
	2625 1325 2975 1325
Wire Wire Line
	2625 1425 2975 1425
Wire Wire Line
	2625 1525 2975 1525
Wire Wire Line
	3475 2125 3475 1875
Wire Wire Line
	3475 1425 3475 1625
Wire Wire Line
	3475 1625 4125 1625
Wire Wire Line
	4125 1625 4125 1325
Wire Wire Line
	3475 1875 4175 1875
Wire Wire Line
	3425 3075 3925 3075
Wire Wire Line
	3875 2025 4175 2025
Wire Wire Line
	4175 2025 4175 1875
$Comp
L power:+5V #PWR01
U 1 1 5DE4586A
P 8950 1200
F 0 "#PWR01" H 8950 1050 50  0001 C CNN
F 1 "+5V" H 8965 1373 50  0000 C CNN
F 2 "" H 8950 1200 50  0001 C CNN
F 3 "" H 8950 1200 50  0001 C CNN
	1    8950 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 1300 8950 1250
Wire Wire Line
	8950 1600 8950 1650
$Comp
L power:GND #PWR02
U 1 1 5DE45872
P 8950 1700
F 0 "#PWR02" H 8950 1450 50  0001 C CNN
F 1 "GND" H 8955 1527 50  0000 C CNN
F 2 "" H 8950 1700 50  0001 C CNN
F 3 "" H 8950 1700 50  0001 C CNN
	1    8950 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5DE45878
P 8650 1450
F 0 "C4" H 8765 1496 50  0000 L CNN
F 1 "1u" H 8765 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8688 1300 50  0001 C CNN
F 3 "~" H 8650 1450 50  0001 C CNN
	1    8650 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 1300 8650 1250
Wire Wire Line
	9250 1250 9250 1300
Wire Wire Line
	8650 1250 8950 1250
Connection ~ 8950 1250
Wire Wire Line
	8950 1250 8950 1200
Wire Wire Line
	8950 1250 9250 1250
Wire Wire Line
	8650 1600 8650 1650
Wire Wire Line
	8650 1650 8950 1650
Connection ~ 8950 1650
Wire Wire Line
	8950 1650 8950 1700
Wire Wire Line
	8950 1650 9250 1650
Wire Wire Line
	9250 1650 9250 1600
$Comp
L Device:C C5
U 1 1 5DE4A476
P 8950 1450
F 0 "C5" H 9065 1496 50  0000 L CNN
F 1 "1u" H 9065 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8988 1300 50  0001 C CNN
F 3 "~" H 8950 1450 50  0001 C CNN
	1    8950 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5DE4A730
P 9250 1450
F 0 "C6" H 9365 1496 50  0000 L CNN
F 1 "1u" H 9365 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9288 1300 50  0001 C CNN
F 3 "~" H 9250 1450 50  0001 C CNN
	1    9250 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0111
U 1 1 5DE584D9
P 7525 1150
F 0 "#PWR0111" H 7525 1000 50  0001 C CNN
F 1 "+5V" H 7540 1323 50  0000 C CNN
F 2 "" H 7525 1150 50  0001 C CNN
F 3 "" H 7525 1150 50  0001 C CNN
	1    7525 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7525 1150 7525 1300
NoConn ~ 2625 2725
NoConn ~ 2625 2825
NoConn ~ 2625 2925
NoConn ~ 2625 3025
NoConn ~ 2625 3125
Wire Wire Line
	5150 1825 5150 1925
Wire Wire Line
	725  875  725  975 
Wire Wire Line
	725  1275 725  1325
Wire Wire Line
	725  1325 1425 1325
Wire Wire Line
	725  1375 725  1325
Connection ~ 725  1325
$Comp
L power:GND #PWR04
U 1 1 5DF11F1B
P 725 1775
F 0 "#PWR04" H 725 1525 50  0001 C CNN
F 1 "GND" H 730 1602 50  0000 C CNN
F 2 "" H 725 1775 50  0001 C CNN
F 3 "" H 725 1775 50  0001 C CNN
	1    725  1775
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5DF17294
P 725 1575
F 0 "SW1" V 725 1527 50  0000 R CNN
F 1 "SW_Push" V 680 1527 50  0001 R CNN
F 2 "Button_Switch_SMD:SW_DIP_SPSTx01_Slide_6.7x4.1mm_W6.73mm_P2.54mm_LowProfile_JPin" H 725 1775 50  0001 C CNN
F 3 "~" H 725 1775 50  0001 C CNN
	1    725  1575
	0    -1   -1   0   
$EndComp
Wire Notes Line
	4325 475  4325 4325
Wire Notes Line
	11225 2375 4325 2375
Wire Notes Line
	6300 475  6300 2375
Text Notes 500  575  0    50   ~ 0
Microcontroller and some RGB LEDs cathodes pins
Text Notes 4350 575  0    50   ~ 0
USB connector used for power supply only
Text Notes 6325 575  0    50   ~ 0
ICSP connector
Text Notes 8300 575  0    50   ~ 0
Decoupling capacitors
Text Notes 4350 2475 0    50   ~ 0
Power transistors for powering every floor (anode) of the LED cube
Wire Notes Line
	8275 475  8275 4325
Wire Notes Line
	475  4325 8275 4325
$EndSCHEMATC
