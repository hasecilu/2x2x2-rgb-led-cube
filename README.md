# 2x2x2 RGB LED CUBE

Small decoration LED cube created just for procrastination purposes.

## Tips

* Use diffused LEDs not clear ones.
* Use tinned cable not copper ones, they will oxidize very quickly.
* Some capacitors are very small, using solder paste is recommended.

## Software used

* [KiCad](https://www.kicad.org/)
* [VSCodium](https://vscodium.com/) + [PlatformIO](https://platformio.org/) extension

## Images

### Render

![Render](images/render.png)

### Bare PCB

![PCB](images/unpopulated.jpg)

### OFF

![OFF](images/OFF.jpg)

### ON

![ON](images/ON.jpg)
