#include "configuration.h"
#include "pinout.h"
#include "LED_functions.h"

/**
 * Function: setupIO
 * --------------------
 * configurates inputs and outputs pins
 *
 * 	returns: nothing
 */

void setupIO(void)
{
	DDRB = 0xC7;		// 1100 0111    LED3    FET0,FET1
	DDRC = 0x3F;		// 0011 1111    LED0,LED1
	DDRD = 0xE0;		// 1110 0000    LED2    

	GATE0_OFF();
	GATE1_OFF();
	for (uint8_t i = 0; i < 12; i++)
		LEDselectorOFF(i);
}
