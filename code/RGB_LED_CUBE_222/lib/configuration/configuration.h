/* 
 * -----------------------------------------------------------------------------
 * File: configuration.h
 * Creation date: 2021-08-14
 * Modification date: 2022-01-10
 * -----------------------------------------------------------------------------
 */

#ifndef CONFIGURATION_H
#define	CONFIGURATION_H

#ifndef F_CPU
#define F_CPU   1000000UL
#endif

// Cube dimensions

#define CUBE_SIZE     2
#define GATES         CUBE_SIZE	// -1 because positions are 0 and 1
#define COLUMNS       CUBE_SIZE
#define ROWS          CUBE_SIZE
#define PWM_DEPTH     128

#define STEPS         25

// Hardware used for logic

#define               COMMON_ANODE_LEDS
//#define               COMMON_CATHODE_LEDS
#define               N_CHANNEL_MOSFET	// LOW_SIDE
//#define               P_CHANNEL_MOSFET  // LOW_SIDE

#ifdef COMMON_ANODE_LEDS

// Need '0' in RGB cathodes to turn on

#define R0_ON()       PORTC &= ~(1<<RED0)
#define R0_OFF()      PORTC |=  (1<<RED0)
#define G0_ON()       PORTC &= ~(1<<GREEN0)
#define G0_OFF()      PORTC |=  (1<<GREEN0)
#define B0_ON()       PORTC &= ~(1<<BLUE0)
#define B0_OFF()      PORTC |=  (1<<BLUE0)

#define R1_ON()       PORTC &= ~(1<<RED1)
#define R1_OFF()      PORTC |=  (1<<RED1)
#define G1_ON()       PORTC &= ~(1<<GREEN1)
#define G1_OFF()      PORTC |=  (1<<GREEN1)
#define B1_ON()       PORTC &= ~(1<<BLUE1)
#define B1_OFF()      PORTC |=  (1<<BLUE1)

#define R2_ON()       PORTD &= ~(1<<RED2)
#define R2_OFF()      PORTD |=  (1<<RED2)
#define G2_ON()       PORTD &= ~(1<<GREEN2)
#define G2_OFF()      PORTD |=  (1<<GREEN2)
#define B2_ON()       PORTD &= ~(1<<BLUE2)
#define B2_OFF()      PORTD |=  (1<<BLUE2)

#define R3_ON()       PORTB &= ~(1<<RED3)
#define R3_OFF()      PORTB |=  (1<<RED3)
#define G3_ON()       PORTB &= ~(1<<GREEN3)
#define G3_OFF()      PORTB |=  (1<<GREEN3)
#define B3_ON()       PORTB &= ~(1<<BLUE3)
#define B3_OFF()      PORTB |=  (1<<BLUE3)

#endif

#ifdef COMMON_CATHODE_LEDS

// Need '1' in RGB cathodes to turn on

#define R0_OFF()      PORTC &= ~(1<<RED0)
#define R0_ON()       PORTC |=  (1<<RED0)
#define G0_OFF()      PORTC &= ~(1<<GREEN0)
#define G0_ON()       PORTC |=  (1<<GREEN0)
#define B0_OFF()      PORTC &= ~(1<<BLUE0)
#define B0_ON()       PORTC |=  (1<<BLUE0)

#define R1_OFF()      PORTC &= ~(1<<RED1)
#define R1_ON()       PORTC |=  (1<<RED1)
#define G1_OFF()      PORTC &= ~(1<<GREEN1)
#define G1_ON()       PORTC |=  (1<<GREEN1)
#define B1_OFF()      PORTC &= ~(1<<BLUE1)
#define B1_ON()       PORTC |=  (1<<BLUE1)

#define R2_OFF()      PORTD &= ~(1<<RED2)
#define R2_ON()       PORTD |=  (1<<RED2)
#define G2_OFF()      PORTD &= ~(1<<GREEN2)
#define G2_ON()       PORTD |=  (1<<GREEN2)
#define B2_OFF()      PORTD &= ~(1<<BLUE2)
#define B2_ON()       PORTD |=  (1<<BLUE2)

#define R3_OFF()      PORTB &= ~(1<<RED3)
#define R3_ON()       PORTB |=  (1<<RED3)
#define G3_OFF()      PORTB &= ~(1<<GREEN3)
#define G3_ON()       PORTB |=  (1<<GREEN3)
#define B3_OFF()      PORTB &= ~(1<<BLUE3)
#define B3_ON()       PORTB |=  (1<<BLUE3)

#endif

#ifdef N_CHANNEL_MOSFET

// Need '1' in MOSFET gate

#define GATE0_OFF()   PORTB &= ~(1<<GATE0)
#define GATE0_ON()    PORTB |=  (1<<GATE0)
#define GATE1_OFF()   PORTB &= ~(1<<GATE1)
#define GATE1_ON()    PORTB |=  (1<<GATE1)

#endif

#ifdef P_CHANNEL_MOSFET

// Need '0' in MOSFET gate

#define GATE0_OFF()   PORTB |=  (1<<GATE0)
#define GATE0_ON()    PORTB &= ~(1<<GATE0)
#define GATE1_OFF()   PORTB |=  (1<<GATE1)
#define GATE1_ON()    PORTB &= ~(1<<GATE1)

#endif

/**
  ******************************************************************************
  * Functions prototypes
  *****************************************************************************/

// Configuration

void setupIO(void);

#endif				// CONFIGURATION_H
