/* 
 * -----------------------------------------------------------------------------
 * File: pinout.h
 * Creation date: 2021-08-14
 * Modification date: 2022-01-24
 * -----------------------------------------------------------------------------
 * 
 * -----------------------------------------------------------------------------
 *                                     ATMEGA8L-8AU
 * 
 *               <->                1 / PD3  PD2 \ 32                <-> 
 *               <->              2 / PD4      PD1 \ 31              <-> 
 *         GND    ->            3 / GND          PD0 \ 30            <-> 
 *         +5V    ->          4 / VCC              PC6 \ 29          <->   RST
 *         GND    ->        5 / GND                  PC5 \ 28        <->   B0
 *         +5V    ->      6 / VCC                      PC4 \ 27      <->   G0
 *       GATE0   <->    7 / PB6                          PC3 \ 26    <->   R0
 *       GATE1   <->  8 / PB7              _|_             PC2 \ 25  <->   B1
 *          B2   <->  9 \ PD5               |              PC1 / 24  <->   G1
 *          G2   <->   10 \ PD6                          PC0 / 23    <->   R1
 *          R2   <->     11 \ PD7                     ADC7 / 22      <-> 
 *          B3   <->       12 \ PB0                 AGND / 21        <-    GND
 *          G3   <->         13 \ PB1             AREF / 20          <-    +5V
 *          R3   <->           14 \ PB2         ADC6 / 19            <-> 
 *               <->             15 \ PB3     AVCC / 18              <-    +5V
 *               <->               16 \ PB4  PB5 / 17                <-> 
 * 
 *                                       TQFP-32
 * -----------------------------------------------------------------------------
 */

/**
 * IMPORTANT NOTE for the firstVersion branch:
 * 
 * D3 in the PCB silkscreen is LED3 in code, #4, PORTB
 * D1 in the PCB silkscreen is LED2 in code, #3, PORTD
 * D2 in the PCB silkscreen is LED1 in code, #2, PORTC
 * D4 in the PCB silkscreen is LED0 in code, #1, PORTC
 * */

#ifndef PINOUT_H
#define PINOUT_H

#include <avr/io.h>

/**
  ******************************************************************************
  * Macros
  *****************************************************************************/

// Output pins

#define GATE0         PB6	// FET0
#define GATE1         PB7	// FET1

#define RED0          PC3	// LED0
#define GREEN0        PC4	// LED1
#define BLUE0         PC5	// LED2

#define RED1          PC0	// LED3
#define GREEN1        PC1	// LED4
#define BLUE1         PC2	// LED5

#define RED2          PD7	// LED6
#define GREEN2        PD6	// LED7
#define BLUE2         PD5	// LED8

#define RED3          PB2	// LED9
#define GREEN3        PB1	// LED10
#define BLUE3         PB0	// LED11

#endif				// PINOUT_H
