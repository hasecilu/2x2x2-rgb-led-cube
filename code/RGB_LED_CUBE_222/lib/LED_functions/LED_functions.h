/* 
 * -----------------------------------------------------------------------------
 * File: LED_functions.h
 * Creation date: 2022-01-12
 * Modification date: 2022-01-12
 * -----------------------------------------------------------------------------
 */

#ifndef LED_FUNCTIONS_H
#define	LED_FUNCTIONS_H

/**
  ******************************************************************************
  * Functions prototypes
  *****************************************************************************/

// LED sequences
void ONtest(uint8_t t);
void floor0(void);
void floor1(void);
void LEDselectorOFF(uint8_t LED);
void LEDselectorON(uint8_t LED);
void allRED_ON(void);
void allRED_OFF(void);
void allGREEN_ON(void);
void allGREEN_OFF(void);
void allBLUE_ON(void);
void allBLUE_OFF(void);
void oneColor(uint8_t point[3]);

#endif				// LED_FUNCTIONS_H
