#include <util/delay.h>

#include "LED_functions.h"
#include "configuration.h"
#include "pinout.h"

/**
 * Function: floor0
 * --------------------
 * turns on the transistor of the first floor
 * and turns off the transistor of the second floor
 *
 * 	returns: nothing
 */

void floor0(void)
{
	GATE0_ON();
	GATE1_OFF();
}

/**
 * Function: floor1
 * --------------------
 * turns on the transistor of the second floor
 * and turns off the transistor of the first floor
 *
 * 	returns: nothing
 */

void floor1(void)
{
	GATE0_OFF();
	GATE1_ON();
}

/**
 * Function: LEDselectorON
 * --------------------
 * multiplexor for turning on the 12 LEDs
 *
 * 	returns: nothing
 */

void LEDselectorON(uint8_t LED)
{
	switch (LED) {
	case 0:
		R0_ON();
		break;
	case 1:
		G0_ON();
		break;
	case 2:
		B0_ON();
		break;
	case 3:
		R1_ON();
		break;
	case 4:
		G1_ON();
		break;
	case 5:
		B1_ON();
		break;
	case 6:
		R2_ON();
		break;
	case 7:
		G2_ON();
		break;
	case 8:
		B2_ON();
		break;
	case 9:
		R3_ON();
		break;
	case 10:
		G3_ON();
		break;
	case 11:
		B3_ON();
		break;
	default:

		break;
	}
}

/**
 * Function: LEDselectorOFF
 * --------------------
 * multiplexor for turning off the 12 LEDs
 *
 * 	returns: nothing
 */

void LEDselectorOFF(uint8_t LED)
{
	switch (LED) {
	case 0:
		R0_OFF();
		break;
	case 1:
		G0_OFF();
		break;
	case 2:
		B0_OFF();
		break;
	case 3:
		R1_OFF();
		break;
	case 4:
		G1_OFF();
		break;
	case 5:
		B1_OFF();
		break;
	case 6:
		R2_OFF();
		break;
	case 7:
		G2_OFF();
		break;
	case 8:
		B2_OFF();
		break;
	case 9:
		R3_OFF();
		break;
	case 10:
		G3_OFF();
		break;
	case 11:
		B3_OFF();
		break;
	default:
		break;
	}
}

/**
 * Function: ONtest
 * --------------------
 * small test for 12*t[ms]
 *
 * 	returns: nothing
 */

void ONtest(uint8_t t)
{
	floor0();
	for (uint8_t i = 0; i < 12; i++) {
		LEDselectorON(i);
		_delay_ms(t);
		LEDselectorOFF(i);
	}

	floor1();
	for (uint8_t i = 0; i < 12; i++) {
		LEDselectorON(i);
		_delay_ms(t);
		LEDselectorOFF(i);
	}
}

void allRED_ON(void)
{
	R0_ON();
	R1_ON();
	R2_ON();
	R3_ON();
}

void allRED_OFF(void)
{
	R0_OFF();
	R1_OFF();
	R2_OFF();
	R3_OFF();
}

void allGREEN_ON(void)
{
	G0_ON();
	G1_ON();
	G2_ON();
	G3_ON();
}

void allGREEN_OFF(void)
{
	G0_OFF();
	G1_OFF();
	G2_OFF();
	G3_OFF();
}

void allBLUE_ON(void)
{
	B0_ON();
	B1_ON();
	B2_ON();
	B3_ON();
}

void allBLUE_OFF(void)
{
	B0_OFF();
	B1_OFF();
	B2_OFF();
	B3_OFF();
}

/**
 * Function: oneColor()
 * --------------------
 * all LEDs turns the same color
 *
 * 	returns: nothing
 */

void oneColor(uint8_t point[3])
{

	if (point[0] > PWM_DEPTH)
		point[0] = 0;
	if (point[1] > PWM_DEPTH)
		point[1] = 0;
	if (point[2] > PWM_DEPTH)
		point[2] = 0;
	if (point[0] < 0)
		point[0] = PWM_DEPTH;
	if (point[1] < 0)
		point[1] = PWM_DEPTH;
	if (point[2] < 0)
		point[2] = PWM_DEPTH;

	for (uint8_t i = 0; i < PWM_DEPTH; i++) {
		for (uint8_t floors = 0; floors < GATES; floors++) {
			if (floors == 0)
				floor0();
			else
				floor1();

			if (i < point[0])
				allRED_ON();
			else
				allRED_OFF();

			if (i < point[1])
				allGREEN_ON();
			else
				allGREEN_OFF();

			if (i < point[2])
				allBLUE_ON();
			else
				allBLUE_OFF();

			_delay_us(30);

			// With 32 bits PWM depth
			// 520[us]*32*2=33.28[ms] -->  30.048[Hz]  Don´t work, the LEDs flick a lot
			// 260[us]*32*2=16.64[ms] -->  60.096[Hz]  It kind of works, the LEDs almost don't flick
			// 130[us]*32*2= 8.32[ms] --> 120.190[Hz]  It kind of works, the LEDs almost don't flick

			// With 128 bits PWM depth
			// 30[us]*128*2= 7.68[ms] --> 130.208[Hz]  It works, the LEDs don't flick
		}
	}
}
