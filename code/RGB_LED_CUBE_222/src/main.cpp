/* 
 * -----------------------------------------------------------------------------
 * File: main.c
 * Creation date: 2021-08-14
 * Modification date: 2021-08-14
 * Author: hasecilu
 * Device: ATMEGA8L-8AU
 * Description: RGB LED 222 CUBE
 * Hardware: 8 RGB LEDs common anode, many resistors, 2 FET transistors
 * -----------------------------------------------------------------------------
 */

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <math.h>

#include "pinout.h"
#include "configuration.h"
#include "LED_functions.h"

typedef enum {
	state0,
	state1,
	state2,
	state3,
	state4
} STATES;

STATES machine_state = state0;

typedef struct {
	STATES state;		// Estado miembro del conjunto enumerado ESTADOS
	void (*func)(int);	// Acciones asociadas al estado          
} FSM;

int main(void)
{
	setupIO();		// Configure output pins

	ONtest(100);

	uint8_t point0[3] = { rand() % PWM_DEPTH, rand() % PWM_DEPTH, rand() % PWM_DEPTH };	// Starting point

	while (1) {
		// If we nest the unitary random generator we could generate bigger pseudo-random numbers
		// The most probable result is 0?, this results in the cube changing very slowly?
		// The implementation shows that changes are not smooth
		// Nice idea, bad implementation
		// With 1 is kind of smooth

		for (uint8_t num = 0; num < 1; num++) {
			// Generate pseudo-randomly a increment, decrement or nothing
			// random number -> -1 or 0 or 1

			for (uint8_t i = 0; i < 2; i++)
				point0[i] += (rand() % 3) - 1;
		}

		for (uint16_t step = 0; step < STEPS; step++)
			oneColor(point0);	//7.68[ms] aprox
	}

	return 0;
}
